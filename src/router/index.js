import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import SignIn from "../views/SignIn.vue";
import SignUp from "../views/SignUp.vue";
import Chat from "../views/Chat";
import store from "../store";
import { renewToken, getJwtFromCookies } from "../services/auth";
import jwt_decode from "jwt-decode";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/sign-in",
    name: "SignIn",
    component: SignIn,
  },
  {
    path: "/sign-up",
    name: "SignUp",
    component: SignUp,
  },
  {
    path: "/chat",
    name: "Chat",
    component: Chat,
    beforeEnter: authorizeRoute,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;

async function authorizeRoute(to, from, next) {
  let redirectToLogin = false;
  if (!store.getters.isAuthenticated) {
    const token = await tryGetAccessToken();
    if (token !== null) {
      store.commit("setAccessToken", token);
      store.dispatch("setRefreshTokenTimer");
    } else {
      redirectToLogin = true;
    }
  }

  if (redirectToLogin) {
    next({
      name: "SignIn",
    });
  } else next();
}

async function tryGetAccessToken() {
  let accessToken = getJwtFromCookies();
  // validate token expiration date
  if (accessToken) {
    const jwtPayload = jwt_decode(accessToken);
    const expTimestamp = jwtPayload.exp * 1000;
    if (expTimestamp <= Date.now() + 60 * 1000) {
      accessToken = null;
    }
  }
  // try to renew token
  if (accessToken === null) {
    const response = await renewToken();
    if (response.requestSuccess) {
      accessToken = response.payload.accessToken;
    }
  }
  return accessToken;
}
