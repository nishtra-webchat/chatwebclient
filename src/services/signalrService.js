import { HubConnectionBuilder, LogLevel } from "@microsoft/signalr";
import store from "../store";
import MessageData from "../models/MessageData.js";
import * as Helpers from "../utils/helpers.js";

const CHAT_HUB_URL = process.env.VUE_APP_SERVER_URL + "/chat";

let connection = null;
let receiveMessageHandlers = new Map([
  [new Object(), (message) => {}], // for type hinting
]);
let addToGroupHandlers = new Map([
  [new Object(), (message) => {}], // for type hinting
]);
let userActivityChangedHandlers = new Map([
  [new Object(), (userId, newActivityStatus) => {}],
]);

/**
 * Establishes connection to the SignalR hub
 */
export async function connect() {
  if (Boolean(connection)) {
    connection.stop();
  }

  connection = new HubConnectionBuilder()
    .withUrl(CHAT_HUB_URL, {
      accessTokenFactory: () => store.state.auth.accessToken,
    })
    .withAutomaticReconnect()
    .configureLogging(LogLevel.Information)
    .build();

  connection.onclose((error) => {
    if (error) {
      console.log(`SignalR connection closed with error: ${error}`);
    }
  });

  connection.on("ReceiveMessage", (message) => {
    Helpers.devlog("Message received: ", message);
    for (const handler of receiveMessageHandlers.values()) {
      handler(message);
    }
  });

  connection.on("AddToGroup", (message) => {
    Helpers.devlog("Message from new contact received: ", message);
    for (const handler of addToGroupHandlers.values()) {
      handler(message);
    }
  });

  connection.on("UserActivityChanged", (userId, status) => {
    for (const handler of userActivityChangedHandlers.values()) {
      handler(userId, status);
    }
  });

  try {
    await connection.start();
    console.log("SignalR Connected.");
  } catch (err) {
    console.log(err);
  }
}

/**
 * Kills connection to the SignalR hub.
 */
export async function disconnect() {
  if (Boolean(connection)) {
    connection.stop();
  }
}

/**
 * Send message. Given a valid messageData the server will process it accordingly
 * (as group or private message) and perform additional actions if required.
 * @param {MessageData} messageData
 * @see {@link MessageData}
 */
export async function sendMessage(messageData) {
  try {
    return await connection.invoke("SendMessage", messageData);
  } catch (err) {
    console.error(err);
  }
}

/**
 * Sends a request to join SignalR hub group for specified ID.
 * It's specifically a SignalR thing and doesn't modify the database.
 * @param {Number} groupId
 */
export async function joinGroup(groupId) {
  try {
    return await connection.invoke("JoinGroup", groupId);
  } catch (err) {
    console.error(err);
  }
}

/**
 * Sends a request to leave SignalR hub group with a specified ID.
 * It's specifically a SignalR thing and doesn't modify the database.
 * @param {Number} groupId
 */
export async function leaveGroup(groupId) {
  try {
    return await connection.invoke("LeaveGroup", groupId);
  } catch (err) {
    console.error(err);
  }
}

// SUBSCRIBE/UNSUBSCRIBE methods
/**
 * Subscribe to receiving messages from SignalR hub.
 * @param {Object} subscriberIdentidier Unique object used to identify the subscriber for the purpose of unsubscribing.
 * @param {Function} handlerFunction Function invoked when the ReceiveMessage event is received.
 */
export function subscribeToReceiveMessage(
  subscriberIdentidier,
  handlerFunction
) {
  receiveMessageHandlers.set(subscriberIdentidier, handlerFunction);
}

/**
 * Unsubscribe from receiving messages from SignalR hub.
 * @param {Object} subscriberIdentidier Unique object used to identify the subscriber.
 */
export function unsubscribeFromReceiveMessage(subscriberIdentidier) {
  receiveMessageHandlers.delete(subscriberIdentidier);
}

/**
 * Subscribe to the event that fires when the current user is added to a new chat group
 * (including being on the receiving end of the new private conversation)
 * @param {Object} subscriberIdentidier Unique object used to identify the subscriber for the purpose of unsubscribing.
 * @param {Function} handlerFunction Function invoked when the AddToGroup event is received.
 */
export function subscribeToAddToGroup(subscriberIdentidier, handlerFunction) {
  addToGroupHandlers.set(subscriberIdentidier, handlerFunction);
}

/**
 * Unsubscribe from the event that fires when the current user is added to a new chat group
 * (including being on the receiving end of the new private conversation)
 * @param {Object} subscriberIdentidier Unique object used to identify the subscriber.
 */
export function unsubscribeFromAddToGroup(subscriberIdentidier) {
  addToGroupHandlers.delete(subscriberIdentidier);
}

/**
 * Subscribe to the event that fires when a user connects/disconnects
 * @param {Object} subscriberIdentidier Unique object used to identify the subscriber for the purpose of unsubscribing.
 * @param {Function} handlerFunction Function invoked when the UserActivityChanged event is received.
 */
export function subscribeToUserActivityChanged(
  subscriberIdentidier,
  handlerFunction
) {
  userActivityChangedHandlers.set(subscriberIdentidier, handlerFunction);
}

/**
 * Unsubscribe from the event that fires when a user connects/disconnects
 * @param {Object} subscriberIdentidier Unique object used to identify the subscriber.
 */
export function unsubscribeFromUserActivityChanged(subscriberIdentidier) {
  userActivityChangedHandlers.delete(subscriberIdentidier);
}
