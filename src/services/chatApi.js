import { ApiResponseModel } from "../models/ApiResponseModel";
import store from "../store";
import * as Helpers from "../utils/helpers.js";

const ENDPOINT = process.env.VUE_APP_SERVER_URL;

function buildUrl(urlPath, searchParams = null) {
  const url = new URL(urlPath, ENDPOINT);
  if (searchParams != null) {
    for (const prop in searchParams) {
      url.searchParams.append(prop, searchParams[prop]);
    }
  }
  return url;
}

function getAccessHeader() {
  return {
    Authorization: "Bearer " + store.state.auth.accessToken,
  };
}

/**
 * Make a request to the server to get conversations the current user is a member of. Will fail for unauthorized users.
 * @returns {Promise<ApiResponseModel>} Server response with success status (true/false) and an optional error message
 * @see {@link ApiResponseModel}
 */
export async function getUserConversations() {
  const url = buildUrl("/api/user-conversations");
  Helpers.devlog(url);
  try {
    const response = await fetch(url, {
      headers: { ...getAccessHeader() },
    });

    if (response.ok) {
      let conversations = await response.json();
      conversations.sort(Helpers.compareConversations);
      return ApiResponseModel.success(conversations);
    } else {
      return ApiResponseModel.fail(
        `Request failed. Status code: ${response.status}`
      );
    }
  } catch (error) {
    return ApiResponseModel.fail(error);
  }
}

/**
 * Make a request to the server to find conversations or users with a name matching a search query. Will fail for unauthorized users.
 * @param {string} searchQuery
 * @returns {Promise<ApiResponseModel>} Server response with success status (true/false) and an optional error message
 * @see {@link ApiResponseModel}
 */
export async function findUsersAndConversations(searchQuery) {
  const url = buildUrl("/api/find", { "search-query": searchQuery });
  Helpers.devlog(url);
  try {
    const response = await fetch(url, {
      headers: { ...getAccessHeader() },
    });

    if (response.ok) {
      let conversations = await response.json();
      conversations.sort(Helpers.compareSearchItems);
      return ApiResponseModel.success(conversations);
    } else {
      return ApiResponseModel.fail(
        `Request failed. Status code: ${response.status}`
      );
    }
  } catch (error) {
    return ApiResponseModel.fail(error);
  }
}

/**
 * Make a request to the server to create a new group
 * @param {string} groupName
 * @returns {Promise<ApiResponseModel>} Server response with success status (true/false) and an optional error message
 * @see {@link ApiResponseModel}
 */
export async function createGroup(groupName) {
  const url = buildUrl("/api/create-conversation", null);
  Helpers.devlog(url);
  try {
    const response = await fetch(url, {
      method: "POST",
      body: JSON.stringify(groupName),
      headers: {
        ...getAccessHeader(),
        "Content-Type": "application/json",
      },
    });

    if (response.ok) {
      let createdGroup = await response.json();
      return ApiResponseModel.success(createdGroup);
    } else {
      if (response.status === 400) {
        return ApiResponseModel.fail("duplicate");
      } else {
        return ApiResponseModel.fail([
          `Request failed. Status code: ${response.status}`,
          `Server response message: ${response.error}`,
        ]);
      }
    }
  } catch (error) {
    return ApiResponseModel.fail(error);
  }
}

/**
 * Make a request to the server to leave the specified group
 * @param {Number} groupName
 * @returns {Promise<ApiResponseModel>} Server response with success status (true/false) and an optional error message
 * @see {@link ApiResponseModel}
 */
export async function leaveGroup(groupId) {
  const url = buildUrl("/api/leave-conversation", null);
  Helpers.devlog(url);
  try {
    const response = await fetch(url, {
      method: "POST",
      body: JSON.stringify(groupId),
      headers: {
        ...getAccessHeader(),
        "Content-Type": "application/json",
      },
    });

    if (response.ok) {
      return ApiResponseModel.success();
    } else {
      return ApiResponseModel.fail([
        `Request failed. Status code: ${response.status}`,
        `Server response message: ${response.error}`,
      ]);
    }
  } catch (error) {
    return ApiResponseModel.fail(error);
  }
}

/**
 * Get last n messages from the server
 *
 * NOTE When connecting to chat group connectToChatGroup method is preferable
 * @param {Number} groupId
 * @param {Number} numberOfMessages
 * @returns {Promise<ApiResponseModel>} Server response with success status (true/false) and an optional error message
 * @see {@link ApiResponseModel}
 */
export async function getLastMessagesForGroup(groupId, numberOfMessages) {
  let requestParams = {
    groupid: groupId,
    n: numberOfMessages,
  };
  const url = buildUrl("/api/latest-messages", requestParams);
  Helpers.devlog(url);
  try {
    const response = await fetch(url, {
      headers: {
        ...getAccessHeader(),
      },
    });

    if (response.ok) {
      let messages = await response.json();
      return ApiResponseModel.success(messages);
    } else {
      return ApiResponseModel.fail([
        `Request failed. Status code: ${response.status}`,
        `Server response message: ${response.error}`,
      ]);
    }
  } catch (error) {
    return ApiResponseModel.fail(error);
  }
}

/**
 * Get n messages sent before a certain time from the server
 * @param {Number} groupId
 * @param {Number} numberOfMessages
 * @param {String} beforeTime Cutout date-time. ASP.NET server provides and accepts date-time as an ISO 8601 formatted string.
 * @returns {Promise<ApiResponseModel>} Server response with success status (true/false) and an optional error message
 * @see {@link ApiResponseModel}
 */
export async function getPrevMessagesForGroup(
  groupId,
  numberOfMessages,
  beforeTime
) {
  let requestParams = {
    groupid: groupId,
    n: numberOfMessages,
    beforetime: beforeTime,
  };
  const url = buildUrl("/api/prev-messages", requestParams);
  Helpers.devlog(url);
  try {
    const response = await fetch(url, {
      headers: {
        ...getAccessHeader(),
      },
    });

    if (response.ok) {
      let messages = await response.json();
      return ApiResponseModel.success(messages);
    } else {
      return ApiResponseModel.fail([
        `Request failed. Status code: ${response.status}`,
        `Server response message: ${response.error}`,
      ]);
    }
  } catch (error) {
    return ApiResponseModel.fail(error);
  }
}

/**
 * Get conversation members
 *
 * NOTE When connecting to chat group connectToChatGroup method is preferable
 * @param {Number} groupId
 * @returns {Promise<ApiResponseModel>} Server response with success status (true/false) and an optional error message
 * @see {@link ApiResponseModel}
 */
export async function getGroupMembers(groupId) {
  let requestParams = {
    groupid: groupId,
  };
  const url = buildUrl("/api/members", requestParams);
  Helpers.devlog(url);
  try {
    const response = await fetch(url, {
      headers: {
        ...getAccessHeader(),
      },
    });

    if (response.ok) {
      let members = await response.json();
      return ApiResponseModel.success(members);
    } else {
      return ApiResponseModel.fail([
        `Request failed. Status code: ${response.status}`,
        `Server response message: ${response.error}`,
      ]);
    }
  } catch (error) {
    return ApiResponseModel.fail(error);
  }
}

/**
 * Notify the server that the current user has opened the specified chat group and request data associated with it.
 * @param {Number} groupId
 * @returns {Promise<ApiResponseModel>} Server response with success status (true/false) and an optional error message.
 * In case of success payload will contain chat group data.
 * @see {@link ApiResponseModel}
 */
export async function connectToChatGroup(groupId) {
  let requestParams = {
    groupid: groupId,
  };
  const url = buildUrl("/api/conversation-connect", requestParams);
  Helpers.devlog(url);
  try {
    const response = await fetch(url, {
      headers: {
        ...getAccessHeader(),
      },
    });

    if (response.ok) {
      let chatGroupData = await response.json();
      return ApiResponseModel.success(chatGroupData);
    } else {
      return ApiResponseModel.fail([
        `Request failed. Status code: ${response.status}`,
        `Server response message: ${response.error}`,
      ]);
    }
  } catch (error) {
    return ApiResponseModel.fail(error);
  }
}

/**
 * Notify the server that the current user has left (stopped browsing) the specified chat group
 * @param {Number} groupId
 * @returns {Promise<ApiResponseModel>} Server response with success status (true/false) and an optional error message
 * @see {@link ApiResponseModel}
 */
export async function disconnectFromChatGroup(groupId) {
  let requestParams = {
    groupid: groupId,
  };
  const url = buildUrl("/api/conversation-disconnect", requestParams);
  Helpers.devlog(url);
  try {
    const response = await fetch(url, {
      headers: {
        ...getAccessHeader(),
      },
    });

    if (response.ok) {
      return ApiResponseModel.success();
    } else {
      return ApiResponseModel.fail([
        `Request failed. Status code: ${response.status}`,
        `Server response message: ${response.error}`,
      ]);
    }
  } catch (error) {
    return ApiResponseModel.fail(error);
  }
}
