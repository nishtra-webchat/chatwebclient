import Vue from "vue";
import Vuex from "vuex";
import auth from "./auth";
import messages from "./messages";
import conversations from "./conversations";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    messages,
    conversations,
  },
});

export default store;
