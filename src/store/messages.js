//import Vuex from "vuex";
import MessageItem from "./../models/MessageItem.js";

/* 
{MessageItem}
  conversationId:1
  id:2101
  senderId:1
  senderName:"admin"
  sentAt:"2021-10-13T15:12:28.2812049Z"
  text:"new message" 
*/

const messages = {
  state: {
    /**
     * @type {MessageItem[]}
     */
    messages: [],
  },
  getters: {
    messages: (state) => state.messages,
  },
  mutations: {
    addMessage(state, message) {
      state.messages.push(message);
    },
    clearMessages(state) {
      state.messages = [];
    },
    setMessageCollection(state, messages) {
      state.messages = messages;
    },
    addEarlierMessages(state, messages) {
      state.messages.splice(0, 0, ...messages);
    },
  },
  actions: {},
};

export default messages;
