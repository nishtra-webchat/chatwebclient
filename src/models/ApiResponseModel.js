export class ApiResponseModel {
  requestSuccess = null;
  error = null;
  payload = null;

  static fail(error) {
    let response = new ApiResponseModel();
    response.requestSuccess = false;
    response.error = error;
    return response;
  }

  static success(payload) {
    let response = new ApiResponseModel();
    response.requestSuccess = true;
    response.payload = payload;
    return response;
  }
}
