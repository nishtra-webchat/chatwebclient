export class MessageData {
  /**
   * Database ID for Conversation. Used in all cases except for a new private chat.
   */
  receiverConversationId = null;
  /**
   * Database ID for User recepient. Used in all private chats.
   */
  receiverUserId = null;
  /**
   * Text content of the message
   */
  messageText = null;

  constructor(receiverConversationId, receiverUserId, messageText) {
    this.receiverConversationId = receiverConversationId;
    this.receiverUserId = receiverUserId;
    this.messageText = messageText;
  }

  static buildMessageToGroup(receiverConversationId, messageText) {
    return new MessageData(receiverConversationId, null, messageText);
  }

  static buildMessageToPrivateChat(
    receiverConversationId,
    receiverUserId,
    messageText
  ) {
    return new MessageData(receiverConversationId, receiverUserId, messageText);
  }

  static buildMessageToPrivateChatNew(receiverUserId, messageText) {
    return new MessageData(null, receiverUserId, messageText);
  }
}
