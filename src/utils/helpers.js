/**
 * Compare two conversation items (rendered in the left sidebar)
 * @param {Object} a
 * @param {Object} b
 * @returns -1 if a goes before b, 1 if a goes after b, 0 if they are equal
 */
export function compareConversations(a, b) {
  let aLastActivity = Date.parse(a.lastMessage?.sentAt);
  let bLastActivity = Date.parse(b.lastMessage?.sentAt);
  aLastActivity = isNaN(aLastActivity) ? Number.MAX_VALUE : aLastActivity;
  bLastActivity = isNaN(bLastActivity) ? Number.MAX_VALUE : bLastActivity;
  let lastActivityCmp = bLastActivity - aLastActivity;

  if (lastActivityCmp !== 0) return lastActivityCmp;
  return a.name.localeCompare(b.name);
}

/**
 * Compare two search items (conversation or user) (rendered in the left sidebar when searching)
 * @param {Object} a
 * @param {Object} b
 * @returns -1 if a goes before b, 1 if a goes after b, 0 if they are equal
 */
export function compareSearchItems(a, b) {
  let isUserCmp = 0;
  if (a.isUser && !b.isUser) isUserCmp = -1;
  else if (!a.isUser && b.isUser) isUserCmp = 1;

  if (isUserCmp !== 0) return isUserCmp;
  return a.name.localeCompare(b.name);
}

/**
 * Format date-time string for chat messages
 * @param {String} dateTime UTC date-time string in ISO 8601 extended format
 * @returns Formatted string
 */
export function formatMessageTime(dateTime) {
  // normalize dateTime string since time received from the server is UTC,
  // but doesn't have a marker or timezone offset,
  if (!dateTime.endsWith("Z")) {
    dateTime += "Z";
  }

  // Works for tested cases. But MDN discourages using strings to instantiate a Date object
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/Date#parameters
  // If errors will be encountered, there will be a need to include a third-party library for working with time.
  let messageDateObj = new Date(dateTime);
  let now = new Date();

  let diff = now - messageDateObj;
  let hours = messageDateObj.getHours();
  let minutes = messageDateObj.getMinutes().toString().padStart(2, "0");
  let timeStr = `${hours}:${minutes}`;

  let passedLessThan24Hours = diff < 24 * 60 * 60 * 1000;
  let isSameDay =
    messageDateObj.getDate() === now.getDate() && passedLessThan24Hours;
  // return only time HH:MM if the message was sent this day
  // e.g. "14:31"
  if (isSameDay) {
    return timeStr;
  }

  // return YYYY/MM/DD, HH:MM if the message wasn't sent today
  // e.g. "2020/10/25, 14:31"
  let year = messageDateObj.getFullYear();
  let month = messageDateObj.getMonth() + 1;
  let day = messageDateObj.getDate();
  return `${year}/${month}/${day}, ${timeStr}`;
}

/**
 * Log to console but only when the app is in the development mode.
 * @param {*} message
 * @param  {...any} optionalParams
 * @returns
 */
export function devlog(message, ...optionalParams) {
  if (process.env.NODE_ENV != "development") return;
  console.log(message, ...optionalParams);
}
